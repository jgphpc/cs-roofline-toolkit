#!/usr/bin/env python3

# copy of roofline method from Python/ert_core.py (keeping gnuplot only)
# TODO: add json output (see self.build_database in Python/ert_core.py)

import sys
import math

from Python.ert_utils import stdout_shell, execute_shell


# {{{ roofline_plot:
def roofline_plot():
    # print("Gathering the final roofline results...")
    # command = "cat %s%s/sum | %s/Scripts/roofline.py"
    command = "cat o.roofline"
    result = stdout_shell(command)
    # (q L1 o.roofline ;q L2 o.roofline ;q dram o.roofline;q flops o.roofline)
    # 13:11508.03 L1 EMP
    # 14: 7601.63 L2 EMP
    # 16: 294.84 DRAM EMP
    # 1:3171.12 FP64 GFLOPs EMP
    # if result[0] != 0:
    #   sys.stderr.write("Unable to create final roofline results\n")
    #   return 1

    lines = result[1].split("\n")
    for i in range(0, len(lines)):
        if len(lines[i].strip()) == 0:
            break

    gflop_lines = lines[:i]
    gbyte_lines = lines[i+1:-1]
    for i in range(0, len(gflop_lines)):
        if gflop_lines[i].strip() == "META_DATA":
            break

    num_peak = i
    gflops_emp = num_peak * [0]
    for i in range(0, num_peak):
        line = gflop_lines[i].split()
        gflops_emp[i] = [float(line[0]), line[1], line[2]]
        # >>> gflops_emp
        # [[3171.12, 'FP64', 'GFLOPs']]

    for i in range(0, len(gbyte_lines)):
        if gbyte_lines[i].strip() == "META_DATA":
            break

    num_mem = i
    gbytes_emp = num_mem * [0]
    for i in range(0, num_mem):
        line = gbyte_lines[i].split()
        gbytes_emp[i] = [float(line[0]), line[1]]
        # >>> gbytes_emp
        # [[11508.03, 'L1'], [7601.63, 'L2'], [3568.67, 'L3'],[294.84, 'DRAM']]

    x = [[0 for i in range(num_peak)] for j in range(num_mem)]
    # [[0], [0], [0], [0]]
    for i in range(0, num_mem):
        for j in range(0, num_peak):
            x[i][j] = gflops_emp[j][0] / gbytes_emp[i][0]
            # [[0.2755571544391177], [0.41716316105887813],
            #  [0.8885999546049368], [10.755392755392755]]
    loadname = "roofline.gnuplot"
    title = "Empirical Roofline Graph"
    xmin = 0.01
    xmax = 100.00
    ymin = 10 ** int(math.floor(math.log10(gbytes_emp[0][0] * xmin)))
    command = (
        f"sed "
        f"-e 's#ERT_TITLE#{title}#g' "
        f"-e 's#ERT_XRANGE_MIN#{xmin}#g' "
        f"-e 's#ERT_XRANGE_MAX#{xmax}#g' "
        f"-e 's#ERT_YRANGE_MIN#{ymin}#g' "
        fr"-e 's#ERT_YRANGE_MAX#\*#g' "
        f"-e 's#ERT_GRAPH#roofline#g' "
        f"< Plot/roofline.gnu.template "
        f"> roofline.gnuplot"
    )
    if execute_shell(command, False) != 0:
        sys.stderr.write("Unable to produce roofline.gnuplot file!\n")
        return 1

    try:
        plotfile = open(loadname, "a")
    except IOError:
        sys.stderr.write("Unable to open '%s'...\n" % loadname)
        return 1

    for h in range(0, num_peak):
        xgflops = 2.0
        label = "%.1f %s/sec (%s Maximum)" % (
            gflops_emp[h][0],
            gflops_emp[h][2],
            gflops_emp[h][1],
        )
        # '3171.1 GFLOPs/sec (FP64 Maximum)'
        rgb = "textcolor rgb '#000080'"
        plotfile.write(
            "set label '%s' at %.7le,%.7le left %s\n"
            % (label, xgflops, 1.2 * gflops_emp[h][0], rgb)
        )

    xleft = xmin
    xright = x[0][0]
    xmid = math.sqrt(xleft * xright)
    ymid = gbytes_emp[0][0] * xmid
    y0gbytes = ymid
    x0gbytes = y0gbytes / gbytes_emp[0][0]
    C = x0gbytes * y0gbytes
    alpha = 1.065
    label_over = True
    # {{{
    for i in range(0, num_mem):
        if i > 0:
            if label_over and gbytes_emp[i-1][0] / gbytes_emp[i][0] < 1.5:
                label_over = False

            if not label_over and gbytes_emp[i-1][0] / gbytes_emp[i][0] > 3.0:
                label_over = True

        if label_over:
            ygbytes = math.sqrt(C * gbytes_emp[i][0]) / math.pow(
                alpha, len(gbytes_emp[i][1])
            )
            xgbytes = ygbytes / gbytes_emp[i][0]

            ygbytes *= 1.1
            xgbytes /= 1.1
        else:
            ygbytes = math.sqrt(C * gbytes_emp[i][0]) / math.pow(
                alpha, len(gbytes_emp[i][1])
            )
            xgbytes = ygbytes / gbytes_emp[i][0]

            ygbytes /= 1.1
            xgbytes *= 1.1

        label = "%s - %.1lf GB/s" % (gbytes_emp[i][1], gbytes_emp[i][0])
        rgb = "textcolor rgb '#800000'"
        plotfile.write(
            "set label '%s' at %.7le,%.7le left rotate by 45 "
            % (label, xgbytes, ygbytes)
        )
        plotfile.write(f"{rgb}\n")
    # }}}
    plotfile.write("plot \\\n")

    # {{{
    for i in range(0, num_mem):
        plotfile.write(
            "     (x <= %.7le ? %.7le * x : 1/0) lc 1 lw 2,\\\n"
            % (x[i][0], gbytes_emp[i][0])
        )
    for j in range(0, num_peak):
        if j == num_peak - 1:
            break
        plotfile.write(
            "     (x >= %.7le ? %.7le : 1/0) lc 3 lw 2,\\\n"
            % (x[0][j], gflops_emp[j][0])
        )
    plotfile.write(
        "     (x >= %.7le ? %.7le : 1/0) lc 3 lw 2\n" %
        (x[0][j], gflops_emp[j][0])
    )
    plotfile.close()
    # }}}

    command = f"echo 'load \"{loadname}\"' |gnuplot "
    print(f"{command}")
    print("Empirical roofline graph: 'roofline.ps'")
# }}}


if __name__ == "__main__":
    roofline_plot()
